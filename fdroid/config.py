repo_url = "https://czlucius.gitlab.io/czlucius-fdroid-repo/fdroid/"
repo_name = "czlucius' F-Droid repo"
repo_icon = "fdroid-icon.png"
repo_description = """
Unofficial Code Scanner repo (.apk files from GitHub Releases)
"""

archive_older = 0

local_copy_dir = "/fdroid"

keystore = "../keystore.bks"
repo_keyalias = "repokey"
