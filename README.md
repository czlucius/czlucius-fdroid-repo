<h2>Add this URL to F-Droid's repositories:
https://czlucius.gitlab.io/czlucius-fdroid-repo/fdroid</h2>
 
Unofficial F-Droid repo for various apps
========================================

Currently included (alphabetically):

* Code Scanner

Add this URL to F-Droid's repositories:
https://czlucius.gitlab.io/czlucius-fdroid-repo/fdroid
<!--[![Repo URL QRcode](fdroid/public/repo-qrcode.png)](https://rfc2822.gitlab.io/fdroid-firefox/fdroid/repo?fingerprint=8F992BBBA0340EFE6299C7A410B36D9C8889114CA6C58013C3587CDA411B4AED)-->

All code and other work in this repository is in the public domain, i.e. licensed as such.<br />
**Both, this GitLab repository and this unofficial F-Droid repository
are not affiliated to, nor have been authorized, sponsored or otherwise approved by the F-Droid developers. This is, however, an official distribution channel for apps by @czlucius.**

You can see what's inside the repo here: https://czlucius.gitlab.io/czlucius-fdroid-repo/fdroid/repo/index.xml

This is based on https://gitlab.com/rfc2822/fdroid-firefox


How does it work?
=================

This unofficial F-Droid repository is hosted using [Gitlab pages](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/).
A daily [scheduled pipeline](https://docs.gitlab.com/ce/user/project/pipelines/schedules.html)
downloads the APKs directly from the app developers and then updates this F-Droid repository (which lives
on Gitlab pages).<br />
All necessary actions are performed by [Gitlab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/).
This Gitlab repository contains the complete source code to configure Gitlab CI/CD and this F-Droid repository.

The private key for signing this unofficial F-Droid repository is kept private, but only used for exactly this purpose.
To generate your own key run:
```
keytool -genkey -v -keystore my.keystore -alias repokey -keyalg RSA -keysize 2048 -validity 10000 -storepass passw0rd1
```

Get base64 representation (ASCII only characters) of the certificate:
```
base64 my.keystore
```
Then paste it into to the gitlab variables.

**The APKs are unaltered and hence still signed by the app developers.**
